bidict==0.21.2
dnspython==1.16.0
eventlet==0.30.2
greenlet==1.0.0
python-engineio==4.1.0
python-socketio==5.2.1
six==1.15.0
